语法
=======

注释
-------

块注释

```
###
SkinnyMochaHalfCaffScript Compiler v1.0
Released under the MIT License
###
```

行注释

```
# inline comment
```

变量范围
-------

> 不需要使用 var 去声明，直接赋值即可定义一个新变量，当一个变量是第一次被赋值时，
> 编译器会在当前范围第一行为其做变量声明，如果一个变量不是第一次被赋值，则不会再做var声明

```
outer = 1
changeNumbers = ->
  inner = -1
  outer = 10
inner = changeNumbers()
```

编译为：

```
var changeNumbers, inner, outer;

outer = 1;

changeNumbers = function() {
  var inner;

  inner = -1;
  return outer = 10;
};

inner = changeNumbers();
```

编译结果：outer只声明一次，儿inner则按出现的顺序和作用域声明了两次

字符串变量
----------

> CoffeeScript可以使用多行字符串，编译时会自动合并为一行,如果不想合并保持多行原有状态，使用首尾三个双引号或三个单引号

```
mobyDick = "Call me Ishmael. Some years ago --
 never mind how long precisely -- having little
 or no money in my purse, and nothing particular
 to interest me on shore, I thought I would sail
 about a little and see the watery part of the
 world..."
```

```
html = """
       <strong>
         cup of coffeescript
       </strong>
       """
```

字符串中使用变量
-----------

> 使用 `#{}`可以在 **双引号字符串**中插入变量和表达式，单引号则保持原样

```
favorite_color = "Blue. No, yel..."
question = "Bridgekeeper: What... is your favorite color? 
Galahad: #{favorite_color}
Bridgekeeper: Wrong!
"

sentence = "#{ 22 / 7 } is a decent approximation of π"
```

编译为：

```
question = "Bridgekeeper: What... is your favorite color? Galahad: " + favorite_color + "Bridgekeeper: Wrong!";
```

多行正则表达式
-----------

> 使用首尾三个斜杠，你可以把正则表达式按多行书写，更加易读

```
OPERATOR = /// ^ (
  ?: [-=]>             # function
   | [-+*/%<>&|^!?=]=  # compound assign / compare
   | >>>=?             # zero-fill right shift
   | ([-+:])\1         # doubles
   | ([&|<>])\2=?      # logic / shift
   | \?\.              # soak access
   | \.{2,3}           # range or splat
) ///
```

Functions
---------

> 函数定义使用 `funcname = (param) -> ` 定义，园括号内定义参数，参数可以定义默认值,如果函数没有参数，可以简写为 `funcname = -> `

> `->` 后面定义函数体，函数体可以一行或多行，如果只有一行，可以写在`->`后面；
> 如果有多行，则`->`后面换行后缩进书写函数体，每行的缩进量必须相同。Coffeescript把最后一行语句编译为 return 语句


###函数体书写

```
square = (x) -> x * x
cube   = (x) -> 
	console.log x
	square(x) * x
```

编译为：

```javascript
var cube, square;

square = function(x) {
  return x * x;
};

cube = function(x) {
  console.log(x);
  return square(x) * x;
};

```

### 参数默认值定义

```
fill = (container, liquid = "coffee") ->
  "Filling the #{container} with #{liquid}..."
```

编译为：

```
var fill;

fill = function(container, liquid) {
  if (liquid == null) {
    liquid = "coffee";
  }
  return "Filling the " + container + " with " + liquid + "...";
};
```

Splats
------

> Splats 是CoffeeScript设计的用来向函数灵活传递参数的语法。在定义function时，首先定义完必须参数之后，
> 可以再定义一个参数，并使用`...`修饰，这类函数在调用时，使用数组作为参数。
> Coffeescript按定义时的参数顺序从数组中先取出需要数量的元素传递给必选参数，然后把只含有剩余的元素的数组传递给Splats参数

> 函数调用时，参数也需要使用`...`修饰

```
gold = silver = rest = "unknown"

awardMedals = (first, second, others...) ->
  gold   = first
  silver = second
  rest   = others

contenders = [
  "Michael Phelps"
  "Liu Xiang"
  "Yao Ming"
  "Allyson Felix"
  "Shawn Johnson"
  "Roman Sebrle"
  "Guo Jingjing"
  "Tyson Gay"
  "Asafa Powell"
  "Usain Bolt"
]

awardMedals contenders...

alert "Gold: " + gold
alert "Silver: " + silver
alert "The Field: " + rest
```

上面的脚本中，awardMedals函数有两个必选参数和一个Splats参数，
contenders数组封装了需要传递给awardMedals函数的参数;当调用时，  "Michael Phelps"传给了first,
"Liu Xiang"传给了second,contenders数组删除前两个元素后，整体传给了others

编译后的js脚本如下：

```
var awardMedals, contenders, gold, rest, silver,
  __slice = [].slice;

gold = silver = rest = "unknown";

awardMedals = function() {
  var first, others, second;

  first = arguments[0], second = arguments[1], others = 3 <= arguments.length ? __slice.call(arguments, 2) : [];
  gold = first;
  silver = second;
  return rest = others;
};

contenders = ["Michael Phelps", "Liu Xiang", "Yao Ming", "Allyson Felix", "Shawn Johnson", "Roman Sebrle", "Guo Jingjing", "Tyson Gay", "Asafa Powell", "Usain Bolt"];

awardMedals.apply(null, contenders);

alert("Gold: " + gold);
alert("Silver: " + silver);
alert("The Field: " + rest);
```

函数调用
---------

> 函数调用时的园括号是可选的，直接用空格将函数名和参数分隔开即可。如果使用函数返回值作为参数，参数函数可以使用圆括号以提高可读性

下面三种写法是等效的：

```
alert inspect a

alert(inspect(a))

alert inspect(a)
```

回调函数
-----------

> 回调函数与函数定义规则相似，只是使用`=>`作为函数体起始符号，也无需 funcname

```
element.addEventListener "click", (e) => 
    console.log(e)
    this.clickHandler(e)
```

编译为

```
element.addEventListener("click", function(e) {
  console.log(e);
  return _this.clickHandler(e);
});
```




对象和数组
----------

> CoffeeScript的数组和对象定义兼容javascript的[]和{}定义语法，当多行书写时，
> 最后的逗号可以省略。对象可以使用类似YAML语法的凹凸缩进方式定义。

> 此外，对象的 key 可以使用javascript保留字

下面的定义都是有效的：

```
song = ["do", "re", "mi", "fa", "so"]

singers = {Jagger: "Rock", Elvis: "Roll"}

singers = Jagger: "Rock", Elvis: "Roll"

bitlist = [
  1, 0, 1
  0, 0, 1
  1, 1, 0
]

kids =
  brother:
    name: "Max"
    age:  11
  sister:
    name: "Ida"
    age:  9
```

编译为

```
var bitlist, kids, singers, song;

song = ["do", "re", "mi", "fa", "so"];

singers = {
  Jagger: "Rock",
  Elvis: "Roll"
};

bitlist = [1, 0, 1, 0, 0, 1, 1, 1, 0];

kids = {
  brother: {
    name: "Max",
    age: 11
  },
  sister: {
    name: "Ida",
    age: 9
  }
};
```

在使用json对象时，同样可以省略大括号


```
$('.account').attr class:'active'

log object.class
```

编译为：

```
$('.account').attr({
  "class": 'active'
});

log(object["class"]);
```

###范围

`range = [1..5]` 等效于 `range = [1,2,3,4,5]`

`range = [1...5]` 等效于 `range = [1,2,3,4]`


###数组和字符串slice

```
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]
start   = numbers[3..5]  #3号索引到5号索引之间，含5号索引
middle  = numbers[3...5] #3号索引到5号索引之间，不含5号索引
end     = numbers[3..]   #3号索引到最后
copy    = numbers[..]    #克隆整个数组
```

编译为：

```
var copy, end, middle, numbers, start;
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
start   = numbers.slice(3, 6);//6-3,三个元素
middle  = numbers.slice(3, 5);//5-3,两个元素
end     = numbers.slice(3);
copy    = numbers.slice(0);
```

`my = "my string"[0..1]` 等效于 `my = "my string".slice(0, 2);`

###判断元素是否在数组中

```
words = ["rattled", "roudy", "rebbles", "ranks"]
alert "Stop wagging me" if "ranks" in words
```

###数组替换

```
numbers = [0..9]
numbers[3..5] = [-3, -4, -5]
```
上面的代码是：把numbers数组的第3到5号索引的元素替换

编译为：

```
var numbers, _ref;
numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
[].splice.apply(numbers, [3, 3].concat(_ref = [-3, -4, -5])), _ref;
```

控制语句
---------

> if/else 语句可以不使用圆括号和大括号，语法类似人类语言，使用的关键字有 if  then  else  

> CoffeeScript 中没有三元表达式，但是可以使用简单的if语句代替

```
if i > 0 
    a="Ok"
else if i<0
    a="Y2K!"
else
    a=0
```

编译为

```
if (i > 0) {
  a = "Ok";
} else if (i < 0) {
  a = "Y2K!";
} else {
  a = 0;
}
```

上面的语句，条件体只有一行，所以可以通过 then 关键字 合写到一行：

```
if i > 0 then a="Ok" else if i<0 then a="Y2K!" else a=0
```

三元表达式也同样是使用 if then else 实现：

```
a= if i > 0 then a="Ok" else a=0
```

编译为：

```
a = i > 0 ? a = "Ok" : a = 0;
```

如果只有一个单行条件体，可以把条件体写在 if 的前面：`alert "It's cold!" if heat < 5` 编译为：`if (heat < 5) {alert("It's cold!");}`

### 条件取反

条件取反除了使用 ! 符号，也可以使用 **not** 关键字

### `!=`, `isnt` 和 `==`,`is`  

`!=`和 **`isnt`**将被编译为 `!==` , `==`和 **`is`**将被编译为 `===`



循环
-------

> 语法  `for VALUE,INDEX in Array/Ranges` 和 `for own VALUE,KEY of Object`

> Value和key可选，用于接收循环到的元素的值和索引，用于循环的可以是数组，json对象，也可以是一个范围表示

> 如果循环体只有一句，可以放在for的前面，写在同一行 , 并且可以在最后使用 when 关键字附加一个条件语句，这个条件语句对前面的单句循环体生效

```
eat food for food in ['toast', 'cheese', 'wine']

courses = ['greens', 'caviar', 'truffles', 'roast', 'cake']
menu i + 1, dish for dish, i in courses

foods = ['broccoli', 'spinach', 'chocolate']
eat food for food in foods when food isnt 'chocolate'
```

编译结果：

```
var courses, dish, food, foods, i, _i, _j, _k, _len, _len1, _len2, _ref;

_ref = ['toast', 'cheese', 'wine'];
for (_i = 0, _len = _ref.length; _i < _len; _i++) {
  food = _ref[_i];
  eat(food);
}

courses = ['greens', 'caviar', 'truffles', 'roast', 'cake'];

for (i = _j = 0, _len1 = courses.length; _j < _len1; i = ++_j) {
  dish = courses[i];
  menu(i + 1, dish);
}

foods = ['broccoli', 'spinach', 'chocolate'];

for (_k = 0, _len2 = foods.length; _k < _len2; _k++) {
  food = foods[_k];
  if (food !== 'chocolate') {
    eat(food);
  }
}
```

> CoffeeScript中可以使用循环方便地从数组或者对象中选择和过滤元素

例如：

```
shortNames = (name for name in list when name.length < 5）
```

上面的语句表示，循环遍历list数组，从中选出字符长度小于5的元素用数组返回赋值给shortNames


> 范围循环可以使用 by 关键字 声明步进，`evens = (x for x in [0..10] by 2)`

### while 循环

> 没有什么特别，单行循环体仍然可以合并到一行书写

> until 等效于 while not ,loop 等效于 while true

```
buy()  while supply > demand
sell() until supply > demand
```

编译为：

```
while (supply > demand) {
  buy();
}

while (!(supply > demand)) {
  sell();
}
```

switch
---------

> switch 语句 使用 when then else 关键字，when 后面可跟多个条件，逗号分隔

```
switch day
  when "Mon" then go work
  when "Tue" then go relax
  when "Thu" then go iceFishing
  when "Fri", "Sat"
    if day is bingoDay
      go bingo
      go dancing
  when "Sun" then go church
  else go work
```

CoffeeScript的switch语句 还可以用来 根据条件返回值

```
score = 76
grade = switch
  when score < 60 then 'F'
  when score < 70 then 'D'
  when score < 80 then 'C'
  when score < 90 then 'B'
  else 'A'
```

语法糖
----------

### `@`表示this

`@saviour = true`  编译为：  `this.saviour = true;`

### `::`表示prototype

```
User::first = -> @records[0]
``` 

编译为：

```
User.prototype.first = function() {
  return this.records[0];
};
```

### `?`

用于修饰变量，表示变量已定义且有值(即类型不为undefined,值不为null)

```
alert brian if brian?
# 编译为
if (typeof brian !== "undefined" && brian !== null) alert(brian);
```

```
velocity = southern ? 40
#编译为：
velocity = typeof southern !== "undefined" && southern !== null ? southern : 40;
```


```
blackKnight.getLegs()?.kick()
#编译为：
if ((_ref = blackKnight.getLegs()) != null) _ref.kick();
```

可以在方法调用前判断成员类型是否是function

```
blackKnight.getLegs().kick?()
#编译为：
if (typeof (_base = blackKnight.getLegs()).kick === "function") _base.kick();
```


### Destructuring Assignment

> [var1,var2...]=[value1,value2...]  {}={}

```
weatherReport = (location) ->
  [location, 72, "Mostly Sunny"]
[city, temp, forecast] = weatherReport "Berkeley, CA"
alert city
```

也可以用来交换变量的值

```
theBait   = 1000
theSwitch = 0

[theBait, theSwitch] = [theSwitch, theBait]
```

支持多层

```
futurists =
  sculptor: "Umberto Boccioni"
  painter:  "Vladimir Burliuk"
  poet:
    name:   "F.T. Marinetti"
    address: [
      "Via Roma 42R"
      "Bellagio, Italy 22021"
    ]

{poet: {name, address: [street, city]}} = futurists
alert city
```

支持splats

```
tag = "<impossible>"

[open, contents..., close] = tag.split ''

alert open
alert contents
alert close
```

Chained Comparisons
------------

> 可以很方便地判断值是否在范围内

```
cholesterol = 127
healthy = "ok"  if 200 > cholesterol > 60
```