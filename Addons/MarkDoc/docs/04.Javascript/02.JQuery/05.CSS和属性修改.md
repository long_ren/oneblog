CLASS修改
==========================

添加和删除class
----------------

> addClass和removeClass方法支持连续操作，参数支持多个空格分割的多个Class

```javascript
$("div").addClass("classname");
$("div").removeClass("classname");
$('p').removeClass('myClass noClass').addClass('yourClass');
```

class开关切换
--------------

> 如果存在，则删除class；如果不存在，则添加class

```javascript
$('body').toggleClass('altStyle');
```

css()方法
==============

> .css()方法用于设置时支持连续操作

读取或修改普通CSS属性
-----------------------

### 读取CSS属性值

```javascript
var bgColor = $('#main').css('background-color');
```

#####{r:注意：}

* css()读取的颜色返回值为rgb(255,10,10,10)形式，且不能识别css中使用百分比的颜色属性值：rgb(100%,10%,0%), HSL (hsl(72,100%,50%)

* css(){b:不能识别css中的短标记}：font,margin,padding,border 等，只能识别font-size,margin-top,padding-top,border-sytle等完整属性

* css()会将百分比数字单位转换为px单位返回。例如返回:12px
 
### 修改CSS属性值

> 修改属性{b:可以使用短标记}

```javascript
$('.pullquote').css('padding',100);
$('p.highlight').css('border', '1px solid black');
```

示例：

```javascript
var baseFont = $('body').css('font-size'); //读取返回值:12px
baseFont = parseInt(baseFont,10); //转换为整数:12
$('body').css('font-size',baseFont * 2+'px');//用表达式修改属性
```

### 修改多个CSS属性

> css()方法的参数支持使用json对象修改多个属性,支持连续操作

```javascript
$('#highlightedDiv').css('background-color','#FF0000') .css('border','2px solid #FE0037');

$('#highlightedDiv').css({
        'background-color' : '#FF0000', 
        'border' : '2px solid #FE0037'
});
```

###   使用回调函数修改css属性值

<p class="api">css(propertyName,function(index,value))</p>

回调函数的index参数接收的是元素在结果集中的索引号,value参数接收的时propertyName的旧值

常用属性快速操作
=================

> 宽度  高度  偏移距离  滚动距离

|@                             |@                                                                                                                                |
|------------------------------|---------------------------------------------------------------------------------------------------------------------------------|
|width([value])                |{g:获取和设置}元素的宽度(支持document，window对象)                                                                                         |
|height([value])               |{g:获取和设置}元素的高度(支持document,window对象 )                                                                                         |
|innerwidth()                  |{g:获取}元素的内部宽度（边框以内，不含边框），无参数                                                                                 |
|innerHeight()                 |{g:获取}元素的内部高度（边框以内，不含边框），无参数                                                                                 |
|outerWidth([boolean])         |{g:获取}元素的外部高度（边框以内，含边框），参数设为true，将把margin值计入                                                           |
|outerHeight([boolean])        |{g:获取}元素的外部高度（边框以内，含边框），参数设为true，将把margin值计入                                                           |
|scrollTop([value])   |{g:获取或设置}从上边界的滚动距离。对有滚动条的元素有效                                                                                |
|scrollLeft([value])  |{g:获取或设置}从左边界的滚动距离。对有滚动条的元素有效                                                                                |
|position()                    |{g:获取}元素相对于父元素的偏移，返回的是一个对象，有两个属性left和top。只对可见元素有效。                                            |
|offset([obj])                 |{g:获取或设置}元素相对于document左上角的偏移，返回的是一个对象，有两个属性left和top。只对可见元素有效。设置时，参数为同样的格式的对象|


元素属性操作
===========

> .attr() 和 .prop()方法

读取和修改元素属性
----------------

> `.attr( attributeName, value )`

> `.attr( map )`

读取：`var imageFile = $('#banner img').attr('src');`

修改：`$('#banner img').attr('src','images/newImage.png');`

##### 注意：{r:attr()方法只会作用于选择结果集的第一个元素；如果属性不存在，返回 undefined}

一次修改多个属性
--------------------

> 参数支持json对象，支持连续操作

```javascript
$('#photo').attr({ 
        src: '1.gif', 
        width: '500px',
        height: '+=10px'
});
```



移除属性
------

> .removeAttr() 和 .removeProp()方法

```javascript
$('body').removeAttr('bgColor');
.removeProp('checked')
```


.attr()和.prop()对比
-------------------

> attr()读取属性的值，而prop()按w3c标准读取DOM属性

例如对于：`<input type="checkbox" checked="checked" />`，使用不同的方式读取得到的返回结果如下：

* `elem.checked`	true (Boolean)
* `$(elem).prop("checked")`	true (Boolean)
* `elem.getAttribute("checked")`	"checked" (String)
* `$(elem).attr("checked")` (1.6)	"checked" (String)
* `$(elem).attr("checked")` (1.6.1+)	"checked" (String)
* `$(elem).attr("checked")` (pre-1.6)	true (Boolean)

{m:✓}是不推荐使用的方法，- 是不支持的方法，黑色对勾是推荐使用的方法

|Attribute/Property|.attr()|.prop()|
|------------------|-------|-------|
|async             |{m:✓}     |✓     |
|autofocus         |{m:✓}     |✓     |
|checked           |{m:✓}     |✓     |
|selected          |{m:✓}     |✓     |
|selectedIndex     |-      |✓     |
|defaultValue      |-      |✓     |
|defaultChecked      |-      |✓     |
|defaultSelected      |-      |✓     |
|location *        |{m:✓}     |✓     |
|multiple          |{m:✓}     |✓     |
|nodeName          |-      |✓     |
|nodeType          |-      |✓     |
|readOnly          |{m:✓}     |✓     |
|tagName           |-      |✓     |
|accesskey         |✓     |-      |
|align             |✓     |-      |
|class             |✓     |-      |
|contenteditable   |✓     |-      |
|draggable         |✓     |-      |
|href              |✓     |-      |
|id                |✓     |-      |
|label             |✓     |-      |
|rel               |✓     |-      |
|src               |✓     |-      |
|style             |✓     |-      |
|tabindex          |✓     |-      |
|title             |✓     |-      |
|type              |✓     |-      |
|width **          |✓     |-      |


`.attr(attributeName,function(index,attr))`
------------------------------------------------

> 用来对结果集中的每个元素修改属性

##### 回调函数中:

this
: 是当前传入的DOM元素

index
: 是该元素在结果集中的索引序号

attr
: 是这个元素的这个属性的旧值

return
: 的字符串作为这个元素这个属性的新值

