状态检测 
---------

<p class="api"> int session_status ( void ）</p>
返回值：
PHP_SESSION_DISABLED     session不可用
PHP_SESSION_NONE           session可用，但没有session数据
PHP_SESSION_ACTIVE         session可用，而且有session数据

session的cookie设置
-----------------

<p class="api">array session_get_cookie_params ( void )</p>
获取session的cookie设置

<p class="api">void session_set_cookie_params (int $lifetime [,string $path [,string $domain[,bool $secure=false[,bool $httponly=false]]]])</p>
设置session的cookie

session基础函数
--------------
<p class="api">bool session_start(void)</p>

启用session

1. 当用户第一次访问时，在客户端cookie生成一个sessionid（默认变量名为“PHPSESSID”，值由php生成一个唯一的字符串），以后访问不再生成sessionid；
2. 客户端访问时将sessionid值传入服务器，便可以从服务器找到对应的session文件读取存储的信息，如果没有找到，则创建文件。

在该函数执行后，只允许session信息的读取，任何session的设置，必须在session_start之前执行，否则无效。

{r:在使用session_start()之前，不允许有任何输出。}

<p class="api">string session_id([$string])</p>

主要用来获取sessionid，在自定义session时用来设置sessionid。

为空表示读取当前用户的sessionid


<p class="api">string session_name(［$string］)</p>

指定session的名称，即客户端cookie[sessionname]

为空或不使用此函数表示默认使用php.ini配置的PHPSESSID,即cookie['PHPSESSID']


<p class="api">bool session_regenerate_id([bool $delete_old_session=false ])</p>

重新产生一个sessionid，必须位于session_start执行之后

参数控制是否删除旧的session（即是否先自动执行session_destroy）

<p class="api">void session_unset ( void )</p>

删除所有session变量；等价于$_SESSION=array()

必须位于session_destroy()之前，否则session_destroy()会删除sessionid,导致session_unset()不知道从内存中删除哪个sessionid下面的变量

<p class="api">bool session_destroy ( void )</p>

删除当前的sessionid以及对应的session文件，但如果访问者客户端cookie仍存有sessionid，当访问网页时，会重新生成原sessionid的文件。

如果需要删除客户端cookie中的sessionid，可以使用`setcookie(session_name(),'',1,'/');`  

满足一定条件下调用此函数(例如：用户退出登录或者登录已超过一定的时间)实现登录失效。

session变量操作
--------------

1. $_SESSION["varname"]

	创建或读取session变量

2. unset($_SESSION["varname"]) 

	删除一个session数据

	注意：不要unset($_SESSION)，这回把全局变量删除，无法使用

	必须位于session_destroy()之前调用

3. $_SESSION = array () 

	把session数据清空
