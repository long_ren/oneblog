# 其他

## 字符填充

<p class="api">
string str_pad ( string $input , int $pad_length [, string $pad_string = " " [, int $pad_type = STR_PAD_RIGHT ]] )
</p>

pad_type
:	参数的可能值为 STR_PAD_RIGHT，STR_PAD_LEFT 或 STR_PAD_BOTH。如果没有指定 pad_type，则假定它是 STR_PAD_RIGHT。

pad_length
:	是填充完以后的总长度。
:	如果填充的长度不能整除单体$pad_string的长度，那么最后一个$pad_string会发生截取，如果最后一个一个$pad_string是多字节字符，就会产生乱码
:	如果 pad_length 的值是负数，小于或者等于输入字符串的长度，不会发生任何填充。

## 重复一个字符串 

<p class="api">
string str_repeat ( string $input , int $multiplier )
</p>

返回 input 重复 multiplier 次后的结果。

iconv多字节字符串函数
------------------

int iconv_strlen ( string $str [, string $charset = ini_get("iconv.internal_encoding") ] )

int iconv_strpos ( string $haystack , string $needle [, int $offset = 0 [, string $charset = ini_get("iconv.internal_encoding") ]] )

int iconv_strrpos ( string $haystack , string $needle [, string $charset = ini_get("iconv.internal_encoding") ] )

string iconv_substr ( string $str , int $offset [, int $length = iconv_strlen($str, $charset) [, string $charset = ini_get("iconv.internal_encoding") ]] )

bool iconv_set_encoding ( string $type , string $charset )

$type参数类型

* input_encoding
* output_encoding
* internal_encoding