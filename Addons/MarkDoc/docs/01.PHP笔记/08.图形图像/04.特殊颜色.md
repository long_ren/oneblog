特殊颜色常量
============

> `IMG_COLOR_TILED ，IMG_COLOR_STYLED ， IMG_COLOR_BRUSHED  ，IMG_COLOR_STYLEDBRUSHED  ，IMG_COLOR_TRANSPARENT(透明色)`


!!!!!任何可使用imagecolorallocate (),imagecolorallocatealpha()返回值的地方都可以使用这些常量

IMG_COLOR_TILED
-------------------

<p class="api">bool imagesettile ( resource $image , resource $tile )</p>

将$tile图像资源设定为$image的贴图，设定后所有颜色填充函数 例如 `imagefill()和 imagefilledpolygon()`都可使用特殊颜色IMG_COLOR_TILED将$tile图像用于填充



IMG_COLOR_STYLED
------------------

<p class="api">bool imagesetstyle ( resource $image , array $style )</p>

将一组颜色组成的点颜色序列与 IMG_COLOR_STYLED 绑定，绑定后画线函数可使用 IMG_COLOR_STYLED 作为颜色。

成功时返回 TRUE， 或者在失败时返回 FALSE.

$style参数中，每个元素都是分配到图像资源上的颜色

```php
/* 画一条虚线，5 个红色像素，5 个白色像素 */
$style = array($red, $red, $red, $red, $red, $w, $w, $w, $w, $w);
```



IMG_COLOR_BRUSHED
---------------------

<p class="api">bool imagesetbrush ( resource $image , resource $brush )</p>

将一副图像绑定到特殊的颜色 IMG_COLOR_BRUSHED ，绑定后画线函数可使用 IMG_COLOR_BRUSHED 绑定的图像作为笔刷绘图

笔刷的行为与各种绘图软件中的笔刷行为是一样的


IMG_COLOR_STYLEDBRUSHED
------------------------

> 当IMG_COLOR_STYLED和IMG_COLOR_BRUSHED都绑定以后，IMG_COLOR_STYLEDBRUSHED便成为有效的特殊颜色可供画线类函数使用，IMG_COLOR_STYLED起到控制比刷间距的作用。

!!!!!该种特殊颜色，只对新建的空白索引画布有效，style序列里面至少有两种颜色，最后一个元素指定的是画布的背景色，其余的任意颜色都代表笔刷图像。


```php
$image = imagecreate(500,500);//不能用真彩色画布   
$bg    = imagecolorallocatealpha($image, 255,255,255,127);//指定画布的背景色
$color = imagecolorallocate($image, 200,0,0);  //随便创建一个颜色来为笔刷占位
   
$style = array($bg,$bg,$bg,$bg,$bg,$bg,$bg,$bg,$bg,$bg,$bg,$bg,$bg,$bg,$bg,$bg,$color);
imagesetstyle($image, $style);//设置划线样式
                                                 
$brush = imagecreatefrompng("logo.png");//笔刷应使用256色以内的索引图像
imagesetbrush($image, $brush);//设置笔刷
                                                 
imageline($image, 100, 280, 400, 220, IMG_COLOR_STYLEDBRUSHED);//划线
header('Content-type:image/png');
imagepng($image);
```

笔刷宽度
-----------

<p class="api">bool imagesetthickness ( resource $image , int $thickness )</p>


设定画线时所用的线宽设为 thickness 个像素。成功时返回 TRUE， 或者在失败时返回 FALSE. 



