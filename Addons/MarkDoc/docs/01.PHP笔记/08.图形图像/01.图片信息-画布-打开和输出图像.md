GD函数规律
-----------

> 除`getimagesize,gd_info,imagesx,imagesy,image_type_to_extension,image_type_to_mime_type`外，其他gd函数都以**image**开头

imagecolor*

imagecreate*

imageset*

imagecopy*

imagefill*

image*


检测图片
--------

<p class="api">array getimagesize ( string $filename)</p>

函数将测定任何 GIF，JPG，PNG，SWF，SWC，PSD，TIFF，BMP，IFF，JP2，JPX，JB2，JPC，XBM，ICON或 WBMP 图像文件的大小,并返回图像的尺寸以及文件类型的int值和一个可以用于普通 HTML 文件中 IMG 标记中的 height/width 文本字符串。  

如果不能访问 filename指定的图像或者其不是有效的图像，getimagesize()将返回 FALSE 并产生一条 E_WARNING 级的错误

该函数是根据文件本身的信息进行判断，而不是根据参数的扩展名判断，所以改扩展名无法欺骗该函数。

Note: 本函数不需要 GD 图像库

!!返回数组中的元素：

* 0 索引是width值

* 1 索引height值

* 2 索引是type（int型），1 = GIF，2 = JPG，3 = PNG，4 = SWF，5 = PSD，6 = BMP，该值用于image_type_to_extension() image_type_to_mime_type()

* 3 索引是用于html的宽高属性字符串内容为“height="yyy" width="xxx"”，

* [mime] mime类型字符串 

* 对于jpg,gif图像，还会返回[channels]、[bits]

#####!!!!!getimagesize的缺陷

随便把一个php文件头部加入一行：GIF89a，扩展名还是php保存，getimagesize仍会认为它是一个gif文件！结果如下：

```php
array(6) {
  [0] =>
  int(15370)
  [1] =>
  int(29800)
  [2] =>
  int(1)
  [3] =>
  string(28) "width="15370" height="29800""
  'channels' =>
  int(3)
  'mime' =>
  string(9) "image/gif"
}
```

所以，对上传的图片，不能仅依靠getimagesize检测合法性。但注意到上面结果中width和height是两个不合常理的极大值，这位我们判断非法gif带来了帮助

安全的检测流程：扩展名检测 >> getimagesize()返回值是否是false >> 对于gif文件width,height是否超宽超高 >> filesize()（文件尺寸明显不合常理）

创建画布 
------------

<p class="api">resource imagecreate ( int $x_size , int $y_size )</p>

imagecreate() 返回一个图像标识符，代表了一幅大小为 x_size 和 y_size 的基于调色板的最多256色图像。 

<p class="api">resource imagecreatetruecolor ( int $x_size , int $y_size )</p>

imagecreatetruecolor() 返回一个图像标识符，代表了一幅大小为 x_size 和 y_size 的真彩色黑色图像（故不能用于gif)。 

<p class="api">resource imagecreatefromgif ( string $filename )</p>

imagecreatefromjpeg() 和 imagecreatefrompng() 

在{y:失败时返回一个空字符串，并且输出一条错误信息}

{r:注意：画布一经建立，色彩模式和尺寸都已经固定，无法改变。}

信息获取
-------------

<p class="api">int imagesx ( resource $image )</p>

imagesx() 返回 image 所代表的图像的宽度。imagesy()返回高度 

<p class="api">string image_type_to_extension (int $imagetype [,bool $include_dot]) </p>
 
根据图片类型的int值，返回图片类型对应的真实扩展名（而不是图片文件本身的扩展名,所以不怕恶意改后缀欺骗），jpg文件返回值为jpeg

{y:返回值带“ . ”号，如：“.png”“.jpeg”}

<p class="api">string image_type_to_mime_type (int $imagetype)</p>

根据图片类型的int值返回图片的mime类型字符串

返回值示例：“image/jpeg”

图像输出和保存
-----------------

<p class="api">bool imagejpeg ( resource $image [,string $filename [,int $quality=75 ]])</p>

* 默认以 JPEG 格式将图像输出到浏览器或写入 filename
* 要省略 filename 参数而提供 quality 参数，使用空字符串（''）
* quality 为可选项，范围从 0（最差质量，文件更小）到 100（最佳质量，文件最大）。

<p class="api">bool imagepng ( resource $image [,string $filename [,int $quality=0 [,int $filters]]])</p>

* 以 PNG 格式将图像输出到浏览器或文件
* 要省略 filename 参数而提供 quality 参数，使用空字符串（''）
* quality范围0-9，默认为0

#####!!filters支持的常量

* PNG_NO_FILTER 
* PNG_FILTER_NONE
* PNG_FILTER_SUB
* PNG_FILTER_UP 
* PNG_FILTER_AVG
* PNG_FILTER_PAETH
* PNG_ALL_FILTERS


<p class="api">bool imagegif ( resource $image [, string $filename ])</p>

图像扩展名获取
--------------------

```php
$im = getimagesize( '/home/zhuyajie/DeepinScrot-5347.png' );
$type = substr( $im['mime'], 6 );//返回：png
$type2 = image_type_to_extension( $im[2] );//返回：.png
//性能相同
```