语法
=======

注释
-----

{*注释内容}

禁止解析
----------

{literal}..{/literal}

定界符输出
------------

{ldelim}，{rdelim}

Smarty变量
-----------

> 支持普通变量，数组，对象(不建议使用)

###类型：

* 标量变量：与php的语法相同，例如`{$foo}`
* 索引数组：与js的数组语法相同，例如`{$foo[0]}`
* 关联数组：与js的对象语法相同，例如`{$foo.bar},{$foo.$bar}`
* 关联数组：php兼容语法`{$foo['bar'][1]}`
* 对象变量：与php语法相同，例如：`{$foo->bar}，{$foo->bar()}`
* 配置文件变量：`{#foo#}` `{$smarty.config.foo}`
* 可变变量：`{$foo_{$x}}`
* section：`{$foo[section_name]}`
* $smarty变量

###配置文件示例：

```conf
# 全局配置变量
pageTitle = "Main Menu"
bodyBgColor = #000000
tableBgColor = #000000
rowBgColor = #00ff00
#区域变量
[Customer]
pageTitle = "Customer Info"
[Login]
pageTitle = "Login"
focus = "username"
#多行字符串变量
Intro = """多行变量值必须
   使用三次双引号."""

# 隐藏区块
[.Database]
host=my.example.com
db=ADDRESSBOOK
user=php-user
pass=foobar
```

###$smarty变量：

* `{$smarty.server.SERVER_NAME}` PHP超全局数组(直接访问超全局变量会弄乱应用程序底层代码和模板语法。 最佳的实践是从PHP将需要的变量对模板进行赋值再使用。)
* `{$smarty.now}` 当前时间戳
* `{$smarty.const}` 直接访问PHP的常量，例如{$smarty.const.MY_CONST_VAL}
* `{$smarty.template}`  模板名称
* `{$smarty.current_dir}` 模板目录
* `{$smarty.version}` Smarty版本号
* `{$smarty.capture}`
* `{$smarty.section}`
* `{$smarty.block.child}`
* `{$smarty.block.parent}`

###分配变量：

{assign var=foo value='baa' nocache}

###数组定义：

* 索引数组：`{assign var=foo value=[1,2,3]}`
* 关联数组：`{assign var=foo value=['y'=>'yellow','b'=>'blue']}`
* 多维数组：`{assign var=foo value=[1,[9,8],3]}`

###数学运算：

任何值为标量的变量和普通值都支持` + - * / % ` 数学运算和函数运算；

例如：`{$x+$y}` `{$foo[$x+3]}` `{$foo = myfunct( ($x+$y)*3 )}`

运算表达式可以用于任何赋值或传值语境

###赋值

赋值符号`=`用于对变量直接赋值，为函数的参数赋值

例如：`{$foo=$bar+2}`，`{assign var=foo value=$x+$y}`

数组变量可追加元素：`{$foo[]=1}`

###php内置函数

{time()}

###双引号中使用变量

在双引号中使用变量最可靠的方式是使用反引号包围变量：`{func var="test\`$foo.bar\` test"}`

如果不使用反引号，则变量只能是标量变量，且无歧义：`{func var="test $foo test"}`  

函数
=======

内置函数(即smarty标签)和自定义函数的用法都是：`{funcname attr1="val1" attr2="val2"}`

attr1,attr2与函数定义的参数名称相同，赋值可以使用任何表达式。如果值为true,例如nocache=true可以简写为nocache

函数调用实例：

```php
{assign var=foo value={counter}}               // 插件结果
{assign var=foo value=substr($bar,2,5)}     // PHP函数结果
{assign var=foo value=$bar|strlen}             // 使用修饰器
{assign var=foo value=$buh+$bar|strlen}  // 复杂的表达式
```
