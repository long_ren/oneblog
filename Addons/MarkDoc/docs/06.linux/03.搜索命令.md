系统文件搜索
=========

which
-----------

> 这个命令从PATH变量设置的目录中搜索文件

* `-a`，搜索出所有的文件，而不止是第一个找到的

whereis
--------------

> 从系统的数据库中搜索出某个文件或目录所在的路径和帮助文档位置，被搜索的文件通常是在线安装的软件的文件以及系统自身的文件，这些文件的保存位置在数据库中有记录

注意：{r:搜索目标必须是完整名称}，且有可能文件实际已经删除也能搜出保存位置，因为数据库没有更新

locate
----------

> 和whereis命令不同在于，它不必使用文件名全称

updatedb 更新系统文件数据库
--------


find
==========

>  `find [PATH] [option] [test] [action]`

option
----------

* `-depth` :递归搜索
* `-maxdepth num`  只对小于等于该深度的文件应用test和action
* `-mindepth num`  不对小于等于该深度的文件应用test和action
* `-daystart` 该选项会作用于后面与时间有关的test(-amin, -atime, -cmin, -ctime, -mmin, and -mtime),启用后相对时间将从今天的开始时刻算起，否则从24小时之前算起。
* `-mount`  不对跨文件系统的文件执行搜索
* `-noleaf` 在cd-rom ms-dos等文件系统搜索时需要使用这个参数
* `-regextype type`  正则类型 默认为emacs风格的正则,可指定为posix-awk, posix-basic, posix-egrep and posix-extended.


test
---------

### 按名称、大小或类型进行搜索

选项与参数：

* `-name  filename`   ：搜寻文件名称为 filename 的文件；可使用shell匹配符号`*,?,[]`。* 匹配0到任意个任意字符，? 匹配单个字符，
* `-iname pattern` ：大小写不敏感
* `-regex pattern`	使用正则表达式搜索文件，可以使用`-regextype type`设定正则模式
* `-iregex pattern` 大小写不敏感
* `-samefile name`	:搜索与name的node相同的文件
* `-size  [+-]SIZE`   ：搜寻比 SIZE 还要大(+)或小(-)的文件。这个 SIZE 的规格有：c: 代表 byte， k: 代表 1024bytes。所以，要找比 50KB还要大的文件，就是『 -size +50k 』
* `-type  TYPE`       ：搜寻文件的类型为 TYPE 的，类型主要有：一般文件 (f),装置文件 (b, c), 目录 (d), 连结档 (l), socket (s), 及 FIFO (p) 等属性。

### 根据时间属性进行搜索

> 与时间有关的option：共有  -amin ,-atime, -ctime , -mtime ,newer  四种。

* `-newer file` ：file 为一个存在的文件， File was modified more recently than file
* `-amin n`: n分钟前访问过的文件
* `-cmin n`: 文件状态在n分钟之前改变过的文件
* `-mmin n`：File's data was last modified n minutes ago.
* `-anewer file` :File was last accessed more recently than file was modified.
* `-cnewer file` : File's status was last changed more recently than file was modified.
* `-used n` ：File was last accessed n days after its status was last changed.
* `-empty` ：空文件和空目录

##### 以 -mtime 说明参数后的数字用法:

* `-mtime  n` ：n 为数字，意义为在 n 天之前的『一天之内』被更动过内容的文件；搜索范围是24个小时
* `-mtime +n` ：列出在 n 天之前(不含 n 天本身)被更动过内容的文件档名；
* `-mtime -n` ：列出在 n 天之内(含 n 天本身)被更动过内容的文件档名。

![find](images/find_time.gif)

### 根据用户属性进行搜索


选项与参数：

* `-uid   n`        ：n 为数字，这个数字是使用者的帐号 ID，亦即 UID ，这个 UID 是记录在/etc/passwd 里面与帐号名称对应的数字。这方面我们会在第四篇介绍。
* `-gid   n`        ：n 为数字，这个数字是群组名称的 ID，亦即 GID，这个 GID 记录在/etc/group，相关的介绍我们会第四篇说明～
* `-user  name`     ：name 为使用者帐号名称喔！例如 dmtsai
* `-group name`     ：name 为群组名称喔，例如 users ；
* `-nouser `        ：寻找文件的拥有者不存在 /etc/passwd 的人！
* `-nogroup`        ：寻找文件的拥有群组不存在於 /etc/group 的文件！ 当你自行安装软件时，很可能该软件的属性当中并没有文件拥有者，这是可能的！在这个时候，就可以使用 -nouser 与 -nogroup 搜寻。


### 按权限属性进行搜索


与文件权限及名称有关的参数：

* `-perm  mode`       ：搜寻文件权限『刚好等於』 mode 的文件，这个 mode 为类似 chmod 的属性值，举例来说， -rwsr-xr-x 的属性为 4755 ！
* `-perm -mode`       ：按权限的下限搜索，大于等于该权限的都会列出
* `-perm +mode`       ：按权限的上限搜索，小于等于该权限的都会列出
* `-executable`
* `-writable`
* `-readable`

ACTION
-----------

### 删除搜索结果

> `-delete`  可以删除搜索出的文件和空目录

使用该action时将自动开启  -depth 选项

### 搜索结果处理


>  `--exec  \;`

```
find / prem +7000  -exec ls -l {} \;
```

这条命令表示将搜索结果发送给`ls -l` 作为参数进行执行，{}用于接收搜索结果
