PDO
======

	PDO {
		static array getAvailableDrivers ( void ) //静态方法，返回支持的驱动__construct (string $dsn [,string $username [,string $password [,array $driver_options ]]])
		bool     inTransaction ( void )
		bool     beginTransaction ( void )
		bool     commit ( void )
		bool     rollBack ( void )

		int      exec (string $statement) //执行非select语句，返回affected rows
		string   lastInsertId ([ string $name = NULL ] ) //返回lastinsertid
		PDOStatement query ( string $statement ) //执行一条sql语句返回结果集，在下一次执行query时，必须fetch了所有的结果，或PDOStatement::closeCursor释放结果
		PDOStatement prepare (string $statement [,array $driver_options = array()]) //准备sql预处理语句
		mixed    errorCode ( void ) //exec和query执行中发生的错误的错误码
		array    errorInfo ( void ) //exec和query执行中发生的错误的错误信息

		mixed    getAttribute ( int $attribute )
		bool     setAttribute ( int $attribute , mixed $value ) //查询时的属性设置
		string   quote ( string $string [, int $parameter_type = PDO::PARAM_STR ] )
	}

PDOStatement
===========

	PDOStatement implements Traversable {
		/* 属性 */
		readonly string $queryString;
		/* 方法 */

		//把预处理占位符与变量$variable的引用绑定，所以绑定变量后，对变量重新重新赋值后可以再次执行excute()。从而实现一条预处理进行多次条件不同的excute
		//使用时要特别当心绑定的$value是否会在其他地方发生不符合预期的修改，尤其是要注意的是，不同的parameter绝对不要使用同一个$variable
		//一个有效避免犯上述错误的的使用方式是，绑定变量之前不要为$variable赋值，绑定以后再赋值
		bool     bindParam(mixed $parameter,mixed &$variable[,int $data_type = PDO::PARAM_STR[,int $length[,mixed $driver_options ]]]) 

		//把预处理占位符设为变量$value的值(不是$value变量自身)，必须先为变量赋值，然后绑定。绑定后预处理解析后是一条固定的sql语句,不能再改变。
		bool     bindValue(mixed $parameter,mixed $value [,int $data_type = PDO::PARAM_STR])    
		bool     bindColumn (mixed $column ,mixed &$param[,int $type [, int $maxlen [,mixed $driverdata ]]]) //绑定字段数据结果到变量,fetch之后用，字段值便自动被赋值给了变量可直接使用赋值后的变量

		bool     execute ([ array $input_parameters ] ) //参数可以为绑定的变量赋值
		string   errorCode ( void ) //prepare执行中发生的错误的错误码
		array    errorInfo ( void ) //prepare执行中发生的错误的错误信息
		int      columnCount ( void )   
		int      rowCount ( void )  //（DELETE, INSERT, or UPDATE）affected rows，对于select查询返回null

		bool     setFetchMode ( int $mode ) //设置fetch和fetchall方法返回的结果形式
		mixed    fetch ([int $fetch_style [,int $cursor_orientation = PDO::FETCH_ORI_NEXT [,int $cursor_offset = 0 ]]] ) //从结果集fetch下一行,默认是PDO::FETCH_BOTH，指针下移一行，如果之前以到最后一行，返回false
		array    fetchAll ([int $fetch_style[,mixed $fetch_argument[,array $ctor_args = array()]]])  //返回所有行，每行都是一个数组或一个对象
		string   fetchColumn ([ int $column_number = 0 ] )            //返回一行中的某个字段值,然后指针移动到下一行
		mixed    fetchObject ([string $class_name="stdClass"[,array $ctor_args]]) //从结果集fetch下一行，返回一个对象（或false）

		mixed    getAttribute ( int $attribute )
		bool     setAttribute ( int $attribute , mixed $value )
		bool     closeCursor ( void ) //在execute另一条预处理语句之前，应调用此方法释放资源。
	}

属性设置常量
===========

<p class="api">void PDO::__construct($dsn[,$username[,$password[,array $driver_options]]])</p>

可以用第四个参数在创建PDO对象时即设定好影响PDO执行方式的属性设置。

数组形式 :array ( PDO::ATTR_*=>$value)，在创建对象后可使用PDO::setAttribute方法修改

<p class="api">bool PDO::setAttribute(int $attribute ,mixed $value)</p>

* PDO::ATTR_PERSISTENT   持久连接

* PDO::ATTR_CASE:  列名大小写属性

	* PDO::CASE_LOWER: 转为小写
	* PDO::CASE_NATURAL: 不转换，默认值
	* PDO::CASE_UPPER: 转为大写
	
* PDO::ATTR_ERRMODE: sql语句执行发生错误的错误报告模式(注意：是SQL语句执行的错误，而不是PDO方法执行时产生的错误)

	* 0或PDO::ERRMODE_SILENT:   默认值，只产生错误码
	* 1或PDO::ERRMODE_WARNING: php的 E_WARNING 报错模式
	* 2或PDO::ERRMODE_EXCEPTION: 抛出异常对象
	
* PDO::ATTR_ORACLE_NULLS (所有驱动都可以用): 如何处理NULL

	* 0或PDO::NULL_NATURAL: 不转换 默认值
	* 1或PDO::NULL_EMPTY_STRING: 空字符串转换为  NULL.
	* 2或PDO::NULL_TO_STRING: NULL转换为空字符串.
	
* PDO::ATTR_STRINGIFY_FETCHES: 是否把数值型数据转换成字符串。属性：布尔值

* PDO::ATTR_STATEMENT_CLASS:  如果要对PDOstatment类进行自定义扩展，用此项告诉PDO扩展类的名称和构造参数.参考http://hi.baidu.com/weiqi228/blog/item/a7c814ad9633260b4a36d67f.html

* PDO::ATTR_TIMEOUT: Specifies the timeout duration in seconds. Not all drivers support this option, and it's meaning may differ from driver to driver. For example, sqlite will wait for up to this time value before giving up on obtaining an writable lock, but other drivers may interpret this as a connect or a read timeout interval. Requires int.

* PDO::ATTR_AUTOCOMMIT (在 OCI, Firebird and MySQL中可用): 是否为单条语句开启自动提交

* PDO::ATTR_EMULATE_PREPARES  是否强制在客户端模拟方式处理预处理语句（默认为true，强制由php进行预处理，性能更好） 设为false则需要驱动原生支持预处理语句，如果驱动不能成功处理则退回进行客户端模拟处理（该常量值不能用getAttribute方法获得）

* PDO::MYSQL_ATTR_USE_BUFFERED_QUERY (available in MySQL): Use buffered queries.

* PDO::ATTR_DEFAULT_FETCH_MODE: 设置默认的fetch模式

* PDO::ATTR_PREFETCH（该常量不能用在getAttribute方法）

* PDO::ATTR_MAX_COLUMN_LEN  （该常量不能用在getAttribute方法）

PDO::setFetchMode
=================

<p class="api">bool PDOStatement::setFetchMode(int $mode)</p>
<p class="api">bool PDOStatement::setFetchMode(int $PDO::FETCH_COLUMN,int $col_number)</p>
<p class="api">bool PDOStatement::setFetchMode(int $PDO::FETCH_CLASS,string $classname,array $construct_arguments)</p>
<p class="api">bool PDOStatement::setFetchMode(int $PDO::FETCH_INTO,object $data_object)</p>


* PDO::FETCH_ASSOC :    关联数组方式返回行记录

* PDO::FETCH_NUM :      索引数组方式返回行记录

* PDO::FETCH_BOTH :     关联和索引元素兼有的数组返回行记录(该项为PDO默认值)

* PDO::FETCH_BOUND :   returns TRUE and assigns the values of the columns in your result set to the PHP variables to which they were bound with the PDOStatement::bindColumn()method

* PDO::FETCH_OBJ :         以(stdClass实例)对象的方式返回行记录，每fetch()一次，返回一个对象。

* PDO::FETCH_LAZY :      可以配合PDO::FETCH_BOTH 和 PDO::FETCH_OBJ使用, 返回的结果中并没有value，只在访问数组或对象中的值的时候才真正去查询数据。

* PDO::FETCH_CLASS :    以自定义类的实例的方式返回行记录( 对象的类应该有__set和__get方法来执行记录的保存和取出)，每fetch()一次，返回一个对象，先赋值到属性中，然后执行构造方法(从而可以使用构造方法修改刚才保存的属性)。

* PDO::FETCH_CLASSTYPE ：需配合 PDO::FETCH_CLASS使用 (e.g. PDO::FETCH_CLASS | PDO::FETCH_CLASSTYPE)，会以第一列的值作为classname

* PDO::FETCH_PROPS_LATE ：需配合 PDO::FETCH_CLASS使用 (e.g. PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE)，会先执行构造方法，然后赋值到属性。

* PDO::FETCH_INTO :      把行记录添加到参数对象中( 对象的类应该有__set和__get方法来执行记录的保存和取出)

* PDO::FETCH_COLUMN  返回某一列的值，如果是fetch，返回的是一个字符串，如果是fetchall返回的是一个一维数组。PDO::FETCH_COLUMN ｜PDO::FETCH_UNIQUE可以过滤fetchall结果中的重复值，PDO::FETCH_COLUMN ｜PDO::FETCH_GROUP可以按该列的值进行分组，即该列中值相同的行作为最终结果的一个数组元素

* PDO::FETCH_FUNC        将一行记录的字段值传入回调函数，由回调函数返回最终的记录

* PDO::FETCH_GROUP     配合其他常量使用

* PDO::FETCH_UNIQUE   即DISTINCT，重复行只返回一行，配合其他常量使用

* PDO::FETCH_KEY_PAIR  如果结果是两列，可以用这个常量，返回一个key-value数组，第一个字段值做key,第二个字段值做value（>php5.2.3）


	```php
	<?php
	//PDO::setFetchMode实例
	class Test
	{
		protected $cols;

		function __set($name, $value) {
			$this->cols[$name] = $value;
		}

		function __get($name) {
			return $this->cols[$name];
		}
	}

	$obj = new Test();
	$db = PDOTest::factory();
	$stmt = $db->prepare("select * from test");
	$stmt->setFetchMode(PDO::FETCH_INTO, $obj);
	$stmt->execute();

	foreach ($stmt as $a) {
		print_r($a);
	}
	print_r($obj); // contains the same values as the last iteration above
	?>
	```

	```php
	<?php
	//PDO::setFetchMode实例
	$stmt = $pdo -> query('your query');
	$stmt -> setFetchMode(PDO::FETCH_CLASS, 'yourClass', array(0 => false));

	while($row = $stmt -> fetch())
	{
	// 变量$row 是"yourClass"类的实例
	}
	$stmt -> closeCursor();
	?>
	```


	```php
	<?php
	//PDO::setFetchMode实例
	class Test
	{
	private $id;
	private $a;
	private $b;

	function __construct ($id, $a, $b)
	{
		$this->id = $id;
		$this->a = $a;
		$this->b = $b;
	}

	public function display()
	{
		echo "<p>id: $this->id, a: $this->a, b: $this->b</p>";
	}
	}

	$testx = new Test(10, 'AAA', 'BBB' );
	$testx->display();
	$testx = NULL;

	try
	{
	$pdo = new PDO('mysql:host=127.0.0.1;dbname=Testing', 'user', 'pswd', array (PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));

	// Prepared statement
	$stmt = $pdo->prepare("SELECT id, a, b FROM test WHERE id = 1");
	$stmt->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, "Test", array('id', 'a', 'b'));
	$stmt->execute();
	foreach ($stmt as $test)
		$test->display();

	// Same again with fetchALL()
	$stmt = $pdo->query("SELECT id, a, b FROM test WHERE id = 2");
	$test = $stmt->fetchALL(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Test', array('id', 'a', 'b'));
	$test[0]->display();

	$pdo = NULL;
	}
	catch (PDOException $e)
	{
	echo 'Error: ', $e->__toString();
	}
	?>
	```

PDO::prepare
============

<p class="api">PDOStatement PDO::prepare($statement[,array $driver_options=array()])</p>

* $driver_optins

	设定execute()方法的参数绑定方式,默认为: `array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL)` 即使用索引数组按顺序为参数传值。

	传入 $driver_optins = array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY) 之后，execute()方法的参数必须使用关联数组以明确的方式为预处理语句传值。

	

	```php
	<?php
	//PDO::prepare参数应用实例
	$sql = 'SELECT name, colour, calories FROM fruit WHERE calories < :calories AND colour = :colour';
	$sth = $dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$sth->execute(array(':calories' => 150, ':colour' => 'red'));
	$red = $sth->fetchAll();
	$sth->execute(array(':calories' => 175, ':colour' => 'yellow'));
	$yellow = $sth->fetchAll();
	?>
	```

	```php
	<?php
	Example #2 Prepare an SQL statement with question mark parameters
	$sth = $dbh->prepare('SELECT name, colour, calories  FROM fruit WHERE calories < ? AND colour = ?');
	$sth->execute(array(150, 'red'));
	$red = $sth->fetchAll();
	$sth->execute(array(175, 'yellow'));
	$yellow = $sth->fetchAll();
	?>
	```


PDO::PARAM_*  常量
==========

> 用在下面的方法中，为绑定的参数明确执行查询时的数据类型

<p class="api">bool PDOStatement::bindParam(mixed $parameter,mixed &$variable[,int $data_type=PDO::PARAM_STR[,int $length[,mixed $driver_options]]])</p>
<p class="api">bool PDOStatement::bindValue(mixed $parameter,mixed $value[,int $data_type=PDO::PARAM_STR])</p>

* $parameter  如果预处理语句用?号占位，则$parameter 使用整数表示第几个占位符号。如果用 :name 方式占位，该参数必须用 :name

* $data_type参数用于明确返回的数据类型，以便php以更合理的方式来处理。

* $length  限制数据的长度


	PDO::PARAM_BOOL(integer)    boolean data type.

	PDO::PARAM_NULL(integer)    SQL NULL data type.

	PDO::PARAM_INT(integer)       SQL INTEGER data type.

	PDO::PARAM_STR(integer)       SQL CHAR,VARCHAR,or other string data type.

	PDO::PARAM_LOB(integer)      大数据对象，以stream方式处理数据

	PDO::PARAM_INPUT_OUTPUT   该常量作为上面的常量的附加项使用，用于指明数据类型只是为输入的$variable设定。


<p class="api">bool PDOStatement::bindColumn(mixed $column,mixed &$param[,int $type[,int $maxlen[,mixed $driverdata]]])</p>

字段绑定的$column可以按顺序也可以按字段名绑定，必须在execute()以后进行绑定，必须使用fetch(PDO::FETCH_BOUND)将数据提取绑定的变量中。



```php
<?php
//PDOStatement::bindParam参数应用实例
/* Call a stored procedure with an INOUT parameter */
$colour = 'red';
$sth = $dbh->prepare('CALL puree_fruit(?)');
$sth->bindParam(1, $colour, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 12);
$sth->execute();
print("After pureeing fruit, the colour is: $colour");
?>
```

```php
<?php
//向浏览器输出LOB数据
$db = new PDO('odbc:SAMPLE', 'db2inst1', 'ibmdb2');
$stmt = $db->prepare("select contenttype, imagedata from images where id=?");
$stmt->execute(array($_GET['id']));
$stmt->bindColumn(1, $type, PDO::PARAM_STR, 256);
$stmt->bindColumn(2, $lob, PDO::PARAM_LOB);
$stmt->fetch(PDO::FETCH_BOUND);

header("Content-Type: $type");
fpassthru($lob);
?>
```

```php
<?php
//保存LOB数据到数据库
$db = new PDO('odbc:SAMPLE', 'db2inst1', 'ibmdb2');
$stmt = $db->prepare("insert into images (id, contenttype, imagedata) values (?, ?, ?)");
$id = get_new_id(); // some function to allocate a new ID

// assume that we are running as part of a file upload form
// You can find more information in the PHP documentation

$fp = fopen($_FILES['file']['tmp_name'], 'rb');

$stmt->bindParam(1, $id);
$stmt->bindParam(2, $_FILES['file']['type']);
$stmt->bindParam(3, $fp, PDO::PARAM_LOB);

$db->beginTransaction();
$stmt->execute();
$db->commit();
?>
```

```php
<?php
//字段绑定
function readData($dbh) {
$sql = 'SELECT name, colour, calories FROM fruit';
try {
	$stmt = $dbh->prepare($sql);
	$stmt->execute();

	/* 按顺序绑定 */
	$stmt->bindColumn(1, $name);
	$stmt->bindColumn(2, $colour);

	/* 按字段名绑定 */
	$stmt->bindColumn('calories', $cals);

	while ($row = $stmt->fetch(PDO::FETCH_BOUND)) {
	$data = $name . "\t" . $colour . "\t" . $cals . "\n";
	print $data;
	}
}
catch (PDOException $e) {
	print $e->getMessage();
}
}
readData($dbh);
?>
```

<p class="api">mixed PDOStatement::fetch([int $fetch_style[,int $cursor_orientation=PDO::FETCH_ORI_NEXT[,int $cursor_offset=0]]])</p>

* $fetch_style 设置返回的结果的形式

* $cursor_orientation 参数可以以指针方式获取记录（prepare()必须使用PDO::ATTR_CURSOR=> PDO::CURSOR_SCROLL）：

	PDO::FETCH_ORI_FIRST  ：fetch第一条记录
	PDO::FETCH_ORI_LAST：fetch最后一条记录
	PDO::FETCH_ORI_NEXT ：fetch下一行记录
	PDO::FETCH_ORI_PRIOR ：fetch上一行记录
	PDO::FETCH_ORI_ABS ：fetch指定行的记录，需在第三个参照中指定偏移量，第一行的偏移量是0
	PDO::FETCH_ORI_REL：相对当前位置，以第三个参数设置的偏移量获取记录

<p class="api">mixed   fetchObject([string $class_name="stdClass"[,array $construct_args]])</p>

等价于 fetch(PDO::FETCH_OBJ)，但 fetchObject 可以指定类名和构造参数

<p class="api">array   fetchAll([int $fetch_style[,mixed $fetch_argument[,array $construct_args=array()]]])</p>

fetchall返回一个数组，数组的每个元素都是一行记录

* $fetch_style   设置每行记录的形式(同fetch()的结果)。
* $fetch_argument 是$fetch_style的辅助参数，$fetch_style为PDO::FETCH_COLUMN，则该参数表示列的索引号(0起始)；$fetch_style为PDO::FETCH_FUNC，则该参数表示函数名；$fetch_style为PDO::FETCH_CLASS，则该参数表示类名，使用第四个参数设置构造参数



	```php
	<?php
	$insert = $dbh->prepare("INSERT INTO fruit(name, colour) VALUES (?, ?)");
	$insert->execute(array('apple', 'green'));
	$insert->execute(array('pear', 'yellow'));

	$sth = $dbh->prepare("SELECT name, colour FROM fruit");
	$sth->execute();

	/* 以第一列为依据分组 */
	var_dump($sth->fetchAll(PDO::FETCH_COLUMN|PDO::FETCH_GROUP));
	?>
	输出:
	array(3) {
	["apple"]=>
	array(2) {
		[0]=>
		string(5) "green"
		[1]=>
		string(3) "red"
	}
	["pear"]=>
	array(2) {
		[0]=>
		string(5) "green"
		[1]=>
		string(6) "yellow"
	}
	["watermelon"]=>
	array(1) {
		[0]=>
		string(5) "green"
	}
	```

	
	
	```php
	<?php
	function fruit($name, $colour) {
		return "{$name}: {$colour}";
	}

	$sth = $dbh->prepare("SELECT name, colour FROM fruit");
	$sth->execute();

	$result = $sth->fetchAll(PDO::FETCH_FUNC, "fruit");
	var_dump($result);
	?>
	输出：
	array(3) {
	[0]=>
	string(12) "apple: green"
	[1]=>
	string(12) "pear: yellow"
	[2]=>
	string(16) "watermelon: pink"
	}
	```

只读常量
===========

> PDO::getAttribute()方法读取

* PDO::ATTR_SERVER_VERSION 输出类似：5.5.24-0ubuntu0.12.04.1
* PDO::ATTR_CLIENT_VERSION 输出类似：mysqlnd 5.0.8-dev - 20102224 - $Id: 65fe78e70ce53d27a6cd578597722950e490b0d0 
* PDO::ATTR_SERVER_INFO     输出类似：Uptime: 140344  Threads: 2  Questions: 445  Slow queries: 0  Opens: 399  Flush tables: 1  Open tables: 98  Queries per second avg: 0.003
* PDO::ATTR_CONNECTION_STATUS  输出类似：Localhost via UNIX socket


需要注意的问题：
=============

先处理异常，最后回滚事务
---------------

```php
<?php
try {
  $dbh = new PDO('odbc:SAMPLE', 'db2inst1', 'ibmdb2', array(PDO::ATTR_PERSISTENT => true));
.......
} catch (Exception $e) {
  //错误的顺序
  $dbh->rollBack();
  echo "Failed: " . $e->getMessage();
}
?>
//上面的catch部分改为下面的顺序
} catch (Exception $e) {
  echo "Failed: " . $e->getMessage();
  $dbh->rollBack();
}
```


下面的代码有什么问题?
----------------

```php
<?php
$dbh = new PDO('mysql:host=localhost;dbname=test', "test");

$query = <<<QUERY
  INSERT INTO `user` (`username`, `password`) VALUES (:username, :password);
QUERY;
$statement = $dbh->prepare($query);

$bind_params = array(':username' => "laruence", ':password' => "weibo");
foreach( $bind_params as $key => $value ){
    $statement->bindParam($key, $value);
}
$statement->execute();
```

答案：经过遍历，:username和:password绑定了同一个变量$value，所以最后execute的时候，:username和:password的值都是$value的值：weibo

```php
<?php
$dbh = new PDO('mysql:host=localhost;dbname=test', "test");

$query = <<<QUERY
  INSERT INTO `user` (`username`, `password`) VALUES (:username, :password);
QUERY;
$statement = $dbh->prepare($query);

$bind_params = array(':username' => "laruence", ':password' => "weibo");
foreach( $bind_params as $key => $value ){
    $statement->bindParam($key, $bind_params[$key]);
}
$statement->execute();
```

安全问题
-----------

PDO是允许一个查询exec()执行多条没有返回结果的sql语句

对于query()、prepare()方法，默认也允许执行多条查询语句的（select语句只有放在第一条才后有返回结果stmt对象，后面的都没有）

如果PDO::ATTR_EMULATE_PREPARES属性被设为0，可以禁止query和prepare方法执行多条语句，因为mysql原生生预处理一次只处理一条。安全性提高了，但这样性能会受影响。

应使用prepare()方法执行绑定用户数据的CURD查询;

exec()用来执行与CRUD无关的sql语句;

query()只用来执行不带用户数据的语句

在调试环境errmode可设为E_warnings，在生产环境errmode应抛出异常

{r:对PDO构造函数一定要捕捉异常，否则出错时，php显示错误信息将暴露完整的__construct内容包括用户名和密码。}
